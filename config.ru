require 'rack'

class MiPrimeraWebApp
  def call(_env)
    [200, { 'Content-type' => 'text/html' }, ['<p> Lorem impsum </p>']]
  end
end

run MiPrimeraWebApp.new
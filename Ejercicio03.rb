class Vehicle
  def initialize(model, year)
    @model = model
    @year = year
    @start = false
  end

  def engine_start
    @start = true
    puts 'El motor se ha encendido!'
  end
end

class Car < Vehicle
  @@instancias = 0

  def initialize
    @@instancias += 1
  end

  def self.number_of_instancies
    @@instancias
  end
end

10.times do
  Car.new.engine_start
end

puts "Instancias generadas de Car: #{Car.number_of_instancies}"
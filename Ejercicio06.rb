module Test
  def result(*args)
    average = ( args[0] + args[1]) / 2.to_f
    if average > 4
      puts 'Estudiante aprobado.'
    else
      puts 'Estudiante reprobado.'
    end
  end
end

module Attendance
  def student_quantity
    "Intancias: #{number_instancies}"
  end
end

class Student
  attr_reader :value1, :value2
  @@quantity = 0
  def initialize(name, value1 = 4, value2 = 4)
    @name = name
    @value1 = value1.to_i
    @value2 = value2.to_i
    @@quantity += 1
  end

  def self.number_instancies
    @@quantity
  end

  include Test # módulo como método de instancia
  extend Attendance # módulo como método de clase
end

# Instancias:
i = 0
10.times do
  Student.new("alumno #{i + 1}", 4, 7)
end

puts "#{Student.student_quantity}" # => Instancias: 10

# Resultados:
student1 = Student.new('Javier', 5, 6)
student1.result(student1.value1, student1.value2) # => Estudiante aprobado.

student2 = Student.new('Juan', 3, 4)
student2.result(student2.value1, student2.value2) # => Estudiante reprobado.